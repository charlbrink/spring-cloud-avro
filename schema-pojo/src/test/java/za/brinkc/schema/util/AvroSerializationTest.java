package za.brinkc.schema.util;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.InputStream;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.avro.Schema;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.avro.AvroFactory;
import com.fasterxml.jackson.dataformat.avro.AvroSchema;
import com.fasterxml.jackson.dataformat.avro.schema.AvroSchemaGenerator;

import za.brinkc.schema.pojo.Person;

public class AvroSerializationTest {

	private static final int AGE = 21;
	private static final String ALYSSA = "Alyssa";
	private static XMLGregorianCalendar dobXml;
	private static XMLGregorianCalendar createdXml;

	@BeforeClass
	public static void initDate() throws DatatypeConfigurationException {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(new Date());
		AvroSerializationTest.dobXml = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
		AvroSerializationTest.createdXml = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
	}
	
	@Test
	public void serializeAndDeserializeWithoutCodeGeneration() throws Exception {
		ClassPathResource resource = new ClassPathResource("avro/Person.avsc");
		InputStream inputStream = resource.getInputStream();

		Schema schema = new Schema.Parser().parse(inputStream);
        
		GenericRecord personNoSchema = new GenericData.Record(schema);
		personNoSchema.put("name", ALYSSA);
		personNoSchema.put("age", AGE);
		LocalDate dob = Instant.ofEpochMilli(dobXml.toGregorianCalendar().getTime().getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
		personNoSchema.put("dateOfBirth", ChronoUnit.DAYS.between(LocalDate.ofEpochDay(0), dob));
		personNoSchema.put("created", createdXml.toGregorianCalendar().getTimeInMillis());

		// serialize
		File file = new File("persons.avro");
		DatumWriter<GenericRecord> datumWriter = new GenericDatumWriter<GenericRecord>(schema);
		DataFileWriter<GenericRecord> dataFileWriter = new DataFileWriter<GenericRecord>(datumWriter);
		dataFileWriter.create(schema, file);
		dataFileWriter.append(personNoSchema);
		dataFileWriter.close();

		// deserialize
		DatumReader<GenericRecord> datumReader = new GenericDatumReader<GenericRecord>(schema);
		DataFileReader<GenericRecord> dataFileReader = new DataFileReader<GenericRecord>(file, datumReader);
		GenericRecord person = null;
		while (dataFileReader.hasNext()) {
			person = dataFileReader.next(person);
			System.out.println("Deserialized WithoutCodeGeneration: " + person);
			assertEquals("Name has changed", ALYSSA, person.get("name").toString());
		}
		dataFileReader.close();

	}

	@Test
	public void serializeAndDeserializeWithCodeGeneration() throws Exception {
		LocalDate dob = Instant.ofEpochMilli(dobXml.toGregorianCalendar().getTime().getTime()).atZone(ZoneId.systemDefault()).toLocalDate();

		Person person = Person.newBuilder().setName(ALYSSA).setAge(AGE)
				.setDateOfBirth((int) ChronoUnit.DAYS.between(LocalDate.ofEpochDay(0), dob))
				.setCreated(createdXml.toGregorianCalendar().getTimeInMillis()).build();

		// serialize
		File file = new File("persons.avro");
		DatumWriter<Person> personDatumWriter = new SpecificDatumWriter<Person>(Person.class);
		DataFileWriter<Person> dataFileWriter = new DataFileWriter<Person>(personDatumWriter);
		dataFileWriter.create(person.getSchema(), file);
		dataFileWriter.append(person);
		dataFileWriter.close();

		// deserialize
		DatumReader<Person> personDatumReader = new SpecificDatumReader<Person>(Person.class);
		DataFileReader<Person> dataFileReader = new DataFileReader<Person>(file, personDatumReader);
		person = null;
		while (dataFileReader.hasNext()) {
			person = dataFileReader.next(person);
			System.out.println("Deserialized WithCodeGeneration: " + person);
			assertEquals("Name has changed", ALYSSA, person.getName().toString());
		}
		dataFileReader.close();
	}

	@Test
	public void serializeAndDeserializeWithoutSchemaFile() throws Exception {
		ObjectMapper mapper = new ObjectMapper(new AvroFactory());
		AvroSchemaGenerator gen = new AvroSchemaGenerator();
		mapper.acceptJsonFormatVisitor(PersonNoSchema.class, gen);
		AvroSchema schemaWrapper = gen.getGeneratedSchema();
		org.apache.avro.Schema schema = schemaWrapper.getAvroSchema();

		GenericRecord personNoSchema = new GenericData.Record(schema);
		personNoSchema.put("name", ALYSSA);
		personNoSchema.put("age", AGE);
		LocalDate dob = Instant.ofEpochMilli(dobXml.toGregorianCalendar().getTime().getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
		personNoSchema.put("dateOfBirth", ChronoUnit.DAYS.between(LocalDate.ofEpochDay(0), dob));
		personNoSchema.put("created", createdXml.toGregorianCalendar().getTimeInMillis());

		// serialize
		File file = new File("persons.avro");
		DatumWriter<GenericRecord> datumWriter = new GenericDatumWriter<GenericRecord>(schema);
		DataFileWriter<GenericRecord> dataFileWriter = new DataFileWriter<GenericRecord>(datumWriter);
		dataFileWriter.create(schema, file);
		dataFileWriter.append(personNoSchema);
		dataFileWriter.close();

		// deserialize
		DatumReader<GenericRecord> datumReader = new GenericDatumReader<GenericRecord>(schema);
		DataFileReader<GenericRecord> dataFileReader = new DataFileReader<GenericRecord>(file, datumReader);
		GenericRecord person = null;
		while (dataFileReader.hasNext()) {
			person = dataFileReader.next(person);
			System.out.println("Deserialized WithoutSchemaFile: " + person);
			assertEquals("Name has changed", ALYSSA, person.get("name").toString());
		}
		dataFileReader.close();
	}

}
