package za.brinkc.schema.util;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlRootElement
public class PersonNoSchema {

	public PersonNoSchema() {}
	
	public PersonNoSchema(String name, int age, XMLGregorianCalendar dateOfBirth, XMLGregorianCalendar created) {
		this.name = name;
		this.age = age;
		this.dateOfBirth = dateOfBirth;
		this.created = created;
	}
	
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateOfBirth;
    @XmlSchemaType(name = "datetime")
    public XMLGregorianCalendar created;

    String name;
	int age;

	public String getName() {
		return name;
	}

	@XmlElement
	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	@XmlElement
	public void setAge(int age) {
		this.age = age;
	}

	public XMLGregorianCalendar getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(XMLGregorianCalendar dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

}