# README #

Example spring cloud stream project using Apache Avro for data serialization.

## How do I get set up? ##

Start Apache [ZooKeeper and Kafka](https://kafka.apache.org/quickstart)

Clone, Build and Run [schema-registry-server](https://bitbucket.org/charlbrink/schema-registry-server)

Clone and Build [spring-cloud-avro](https://bitbucket.org/charlbrink/spring-cloud-avro)

> git clone git@bitbucket.org:charlbrink/spring-cloud-avro.git
>
> cd spring-cloud-avro
>
> mvn clean install

Run schema-registry-server

> cd schema-registry-server
>
> mvn spring-boot:run

Run schema-registry-consumer

> cd schema-registry-consumer
>
> mvn spring-boot:run

Run (optional/alternative consumer) kafka-console-consumer.sh

> ./bin/kafka-console-consumer.sh --topic test --zookeeper localhost:2181

Run schema-registry-producer

> cd schema-registry-producer
>
> mvn spring-boot:run
