package za.brinkc.schema.registry.producer;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface Channels {
	
	@Output("users")
	MessageChannel users();
}
