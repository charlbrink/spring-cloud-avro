package za.brinkc.schema.registry.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.schema.client.EnableSchemaRegistryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.support.MessageBuilder;

import example.avro.User;

@EnableBinding(Channels.class)
@EnableSchemaRegistryClient
@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	@InboundChannelAdapter("users")
	public MessageSource<User> userMessageSource() {
		User user = User.newBuilder().setName("John Doe").setFavoriteColor("green")
		        .setFavoriteNumber(null).build();

		return () -> MessageBuilder.withPayload(user).build();
	}	
	
}
