package za.brinkc.schema.registry.consumer;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface Channels {
	
	@Input("users")
    SubscribableChannel users();
}
