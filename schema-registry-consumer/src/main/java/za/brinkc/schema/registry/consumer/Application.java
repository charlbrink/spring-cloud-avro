package za.brinkc.schema.registry.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.schema.client.EnableSchemaRegistryClient;

import example.avro.User;

@EnableBinding(Channels.class)
@EnableSchemaRegistryClient
@SpringBootApplication
public class Application {
	Logger log = LoggerFactory.getLogger(Application.class);
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@StreamListener("users")
	public void handle(User user) {
		log.info("Received: "+user);;
	}
	
}
